#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <algorithm>
#include <array>
#include <unistd.h>


#ifdef _WIN32
#include <windows.h>
#include <ShellAPI.h>
#else
#include <cstdlib>
#endif

using namespace std;

unsigned int offset_characters_position=16, offset_characters_length=8, table_start_dec=11;
string sfkString="sfk",xddString="xxd";

string int_to_hex(unsigned int value,bool prefix){
    stringstream stream;
    stream <<hex<<value;
    string result(stream.str());
    transform (result.begin(), result.end(), result.begin(), ::toupper);
    if (prefix){
        result.insert(0,"0x");
    }
    return result;
}

#ifdef _WIN32
void executeCommand(string exec,string parameters){
    SHELLEXECUTEINFO ShExecInfo = {0};
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = exec.c_str();
    ShExecInfo.lpParameters = parameters.c_str();
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_HIDE;
    ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo);
    WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
    CloseHandle(ShExecInfo.hProcess);
}

int dumpBinaryToHex(string rawFile,string tempPath){

    string commandDumpBinToHex="/C sfk.exe hexdump -pure -nofile -nolf ";
    commandDumpBinToHex.append(rawFile);
    commandDumpBinToHex.append(" > ");
    commandDumpBinToHex.append(tempPath);

   // ShellExecute(NULL,"open", "cmd.exe", commandDumpBinToHex.c_str(), 0, SW_HIDE);

    executeCommand("cmd.exe",commandDumpBinToHex);

    ifstream hexRead {tempPath };
    string stringHexRead {istreambuf_iterator<char>(hexRead), istreambuf_iterator<char>() };
    hexRead.close();

    if(!(stringHexRead.size()>0)){
        return -1;
    }

    return 0;
}

int hexDumpToBinary(string hexdump_path,string temp_bin_path){

    string command_hexdump_to_binary("/C sfk.exe filter ");
    command_hexdump_to_binary.append(hexdump_path);
    command_hexdump_to_binary.append(" +hextobin ");
    command_hexdump_to_binary.append(temp_bin_path);

    executeCommand("cmd.exe",command_hexdump_to_binary);

    ifstream hexRead {hexdump_path };
    string stringHexRead {istreambuf_iterator<char>(hexRead), istreambuf_iterator<char>() };
    hexRead.close();

    //If size 0(false), negated (true)
    if(!stringHexRead.size()){
        return -1;
    }

    return 0;
}

#else
int executeCommand(string exec,string parameters){
    array<char, 128> buffer;
    string result;

    string command=exec+" "+parameters;    
    FILE* fp = popen(command.c_str(),"r");
    if (!fp)
    {
        cerr << "Couldn't start command." << endl;
        return -1;
    }

    return pclose(fp);
}
int dumpBinaryToHex(string rawFile,string tempPath){

    string commandDumpBinToHex="-p ";
    commandDumpBinToHex.append(rawFile);
    commandDumpBinToHex.append(" > ");
    commandDumpBinToHex.append(tempPath);

    if(executeCommand(xddString,commandDumpBinToHex)==-1){
        return -1;
    }
    return 0;
}

int hexDumpToBinary(string hexdump_path,string temp_bin_path){

    string command_hexdump_to_binary(" filter ");
    command_hexdump_to_binary.append(hexdump_path);
    command_hexdump_to_binary.append(" +hextobin ");
    command_hexdump_to_binary.append(temp_bin_path);

    if(executeCommand(sfkString,command_hexdump_to_binary)==-1){
        return -1;
    }
    return 0;
}
#endif

void pad_chars(string &str,string ch, unsigned int len,bool start){
     for(unsigned int i=0;str.size()<len;i++){
          if(start){
          str.insert(0,ch);
          }
          else{
               str.insert(str.size(),ch);
          }
     }
}

string string_endianess_reverse(string stringHexIn){
    string reversed_string="";

    if(stringHexIn.size()%2){
         pad_chars(stringHexIn,"0",stringHexIn.size()+1,true);
    }

    for (int i=0;i<stringHexIn.length();i+=2) {
        reversed_string.insert(0, stringHexIn.substr(i,2));
    }

    return reversed_string;
}


int hex_to_int(string hex){
    int x=stoul(string("0x").append(string_endianess_reverse(hex)),nullptr,16);

    return x;
}

void findAndReplace(string & data, string toSearch, string replaceStr, bool all)
{
    // Get the first occurrence
    size_t pos = data.find(toSearch);
    // Repeat till end is reached
    while( pos != string::npos)
    {
        // Replace this occurrence of Sub String
        data.replace(pos, toSearch.size(), replaceStr);
        if(!all){
            return;
        }
        // Get the next occurrence from the current position
        pos =data.find(toSearch, pos + replaceStr.size());
    }
}

void writeFile(ostream &fileToWrite,string stringToWrite){
    fileToWrite << stringToWrite;
}

int main (int argc, char *argv[]){

    if(argc!=2){
        cout <<"Wrong params!"<<endl;
        return -1;
    }

    string raw_file_path=argv[1];
    string temp_path_dump=raw_file_path+"_dump.txt";

    if (dumpBinaryToHex(raw_file_path,temp_path_dump)==-1){
        cout <<"Error dumping binary to hex"<<endl;
        return -1;
    }
    
    ifstream file_stream {temp_path_dump};
    string raw_file_h {istreambuf_iterator<char>(file_stream), istreambuf_iterator<char>() };
    file_stream.close();
    
    string offset_reverse_hex=raw_file_h.substr(offset_characters_position,offset_characters_length);
    unsigned int offset_dec=hex_to_int(offset_reverse_hex);
    string offset_hex=int_to_hex(offset_dec,false);

    unsigned int file_length_dec=raw_file_h.size();
    string file_length_hex=int_to_hex(file_length_dec/2,false);

    unsigned int correct_table_size_dec=(file_length_dec/2)-table_start_dec-1;
    string correct_table_size_hex=int_to_hex(correct_table_size_dec,false);

    string correct_table_size_reverse_hex=string_endianess_reverse(correct_table_size_hex);

    pad_chars(correct_table_size_reverse_hex,"0",8,false);

    findAndReplace(raw_file_h,offset_reverse_hex,correct_table_size_reverse_hex,false);

    string temp_patched_hex=raw_file_path+"_patched_hex.txt";
    
    ofstream patched_hex_stream;
    patched_hex_stream.open(temp_patched_hex);

    writeFile(patched_hex_stream,raw_file_h);
    patched_hex_stream.close();

    string temp_patched_bin=raw_file_path;

    if (hexDumpToBinary(temp_patched_hex,temp_patched_bin)==-1){
        cout<<"Error dumping hex to binary"<<endl;
        return -1;
    }
    return 0;
}